import React, { Component } from "react";
import List_Shoe from "./List_Shoe";
import Shoe_Cart from "./Shoe_Cart";
import { data_shoe } from "./Data_List";

class Shoe_Shop extends Component {
  state = {
    data_list_shoe: data_shoe,
    cart: [],
  };
  handleAddToCart = (shoe) => {
    let id = shoe.id;
    let viTri = this.state.cart.findIndex((item) => item.id == id);
    if (viTri != -1) {
      this.state.cart[viTri].soLuong++;
      this.setState({
        cart: this.state.cart,
      });
    } else {
      this.setState({
        cart: [...this.state.cart, shoe],
      });
    }
  };
  handXoaItem = (id) => {
    const filteredItems = this.state.cart.filter((item) => item.id != id);
    this.setState({
      cart: filteredItems,
    });
  };
  handleThemItem = (id) => {
    let viTri = this.state.cart.findIndex((item) => item.id == id);
    this.state.cart[viTri].soLuong++;
    this.setState({
      cart: this.state.cart,
    });
  };
  handleGiamItem = (id) => {
    let viTri = this.state.cart.findIndex((item) => item.id == id);
    if (this.state.cart[viTri].soLuong > 1) {
      this.state.cart[viTri].soLuong--;
      this.setState({
        cart: this.state.cart,
      });
    }
  };
  render() {
    return (
      <div>
        <Shoe_Cart
          themItem={this.handleThemItem}
          giamItem={this.handleGiamItem}
          deleteItem={this.handXoaItem}
          carts={this.state.cart}
        />
        <List_Shoe
          addToCart={this.handleAddToCart}
          shoes={this.state.data_list_shoe}
        />
      </div>
    );
  }
}

export default Shoe_Shop;
