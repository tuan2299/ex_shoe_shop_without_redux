import React, { Component } from "react";

class Shoe_Cart extends Component {
  RenderCartWithMap = () => {
    return this.props.carts.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                {
                  this.props.giamItem(item.id);
                }
              }}
              className="ml-2 btn btn-outline-info minus">
              -
            </button>
            {item.soLuong}
            <button
              onClick={() => {
                {
                  this.props.themItem(item.id);
                }
              }}
              className="mr-2 btn btn-outline-info plus">
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.deleteItem(item.id);
              }}
              className="btn btn-danger">
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container p-5">
        <h2 className="text-center mb-5">Shoe Cart</h2>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Image</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.RenderCartWithMap()}</tbody>
        </table>
      </div>
    );
  }
}

export default Shoe_Cart;
