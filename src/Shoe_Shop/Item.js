import React, { Component } from 'react';

class Item extends Component {
    render() {
        let {name , price , image} = this.props.item
        return (
            <div className='col-4 mb-3'>
                <div className="card text-left">
                    <img className="card-img-top" src={image} />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">{price}</p>
                        <button onClick={this.props.addToCart} className="btn btn-info">Add to Cart</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Item;